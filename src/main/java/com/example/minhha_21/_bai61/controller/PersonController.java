package com.example.minhha_21._bai61.controller;

import com.example.minhha_21._bai61.edu.Student;
import com.example.minhha_21._bai61.edu.Teacher;
import com.example.minhha_21._bai61.model.ResponseApi;
import com.example.minhha_21._bai61.service.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
@RequestMapping("/person")
public class PersonController {
    @Autowired
    private Services services;

    @GetMapping("/getPerson")
    public ResponseEntity<ResponseApi> getStudent() {
        return new ResponseEntity<>(services.getPerson(), HttpStatus.OK);
    }

    @PostMapping("/addStudent")
    public ResponseEntity<ResponseApi> addStudent(@RequestBody Student student) {
        return new ResponseEntity<>(services.addStudent(student), HttpStatus.OK);
    }

    @PutMapping("/updateStudent/{index}")
    public ResponseEntity<ResponseApi> updateStudent(@PathVariable int index, @RequestBody Student student) {
        return new ResponseEntity<>(services.updateStudent(index, student), HttpStatus.OK);
    }

    @DeleteMapping("/deleteStudent/{index}")
    public ResponseEntity<ResponseApi> deleteStudent(@PathVariable int index) {
        return new ResponseEntity<>(services.deleteStudent(index), HttpStatus.OK);
    }

    @PostMapping("/addTeacher")
    public ResponseEntity<ResponseApi> addTeacher(@RequestBody Teacher teacher) {
        return new ResponseEntity<>(services.addTeacher(teacher), HttpStatus.OK);
    }

    @PutMapping("/updateTeacher/{index}")
    public ResponseEntity<ResponseApi> updateTeacher(@PathVariable int index, @RequestBody Teacher teacher) {
        return new ResponseEntity<>(services.updateTeacher(index, teacher), HttpStatus.OK);
    }

    @DeleteMapping("/deleteTeacher/{index}")
    public ResponseEntity<ResponseApi> deleteTeacher(@PathVariable int index) {
        return new ResponseEntity<>(services.deleteTeacher(index), HttpStatus.OK);
    }
}
