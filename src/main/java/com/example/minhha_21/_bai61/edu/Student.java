package com.example.minhha_21._bai61.edu;

import lombok.Data;

@Data
public class Student extends Person {
    private String tenLop;
    private float diem;
    public Student() {}
    public Student(String tenLop) {
        this.tenLop = tenLop;
    }
    public Student(float diem) {
        this.diem = diem;
    }
    public Student(String tenLop, float diem) {
        this.tenLop = tenLop;
        this.diem = diem;
    }
    public Student(String ten, int tuoi, String gioiTinh) {
        super(ten, tuoi, gioiTinh);
    }
    public Student(String ten, int tuoi, String gioiTinh, String tenLop, float diem) {
        super(ten, tuoi, gioiTinh);
        this.tenLop = tenLop;
        this.diem = diem;
    }
}
