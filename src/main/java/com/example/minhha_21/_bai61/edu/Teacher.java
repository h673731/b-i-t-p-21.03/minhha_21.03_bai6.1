package com.example.minhha_21._bai61.edu;

import lombok.Data;

@Data
public class Teacher extends Person {
    private int namNgheNghiep;
    private String tinhTrangHonNhan;
    public Teacher() {}
    public Teacher(int namNgheNghiep) {
        this.namNgheNghiep = namNgheNghiep;
    }
    public Teacher(int namNgheNghiep, String tinhTrangHonNhan) {
        this.namNgheNghiep = namNgheNghiep;
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }
    public Teacher(String ten, int tuoi, String gioiTinh) {
        super(ten, tuoi, gioiTinh);
    }
    public Teacher(String ten, int tuoi, String gioiTinh, int namNgheNghiep, String tinhTrangHonNhan) {
        super(ten, tuoi, gioiTinh);
        this.namNgheNghiep = namNgheNghiep;
        this.tinhTrangHonNhan = tinhTrangHonNhan;
    }
}
