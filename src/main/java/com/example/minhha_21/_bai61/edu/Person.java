package com.example.minhha_21._bai61.edu;

import lombok.Data;

@Data
public abstract class Person {
    private String ten;
    private int tuoi;
    private String gioiTinh;
    public Person() {}
    public Person(String ten) {
        this.ten = ten;
    }
    public Person(String ten, int tuoi) {
        this.ten = ten;
        this.tuoi = tuoi;
    }
    public Person(String ten, int tuoi, String gioiTinh) {
        this.ten = ten;
        this.tuoi = tuoi;
        this.gioiTinh = gioiTinh;
    }
    public String printHello() {
        return "Hello, I am a Person";
    }
}
