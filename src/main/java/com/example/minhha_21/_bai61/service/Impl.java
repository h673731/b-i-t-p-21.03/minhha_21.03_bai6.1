package com.example.minhha_21._bai61.service;

import com.example.minhha_21._bai61.edu.Student;
import com.example.minhha_21._bai61.edu.Teacher;
import com.example.minhha_21._bai61.model.ResponseApi;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Impl implements Services {

    private final List<Student> studentList = new ArrayList<>();
    private final List<Teacher> teacherList = new ArrayList<>();

    @Override
    public ResponseApi getPerson() {
        try {
            Map<String, List<?>> listData = new HashMap<>();
            listData.put("Student List", studentList);
            listData.put("Teacher List", teacherList);
            int countObject = studentList.size() + teacherList.size();
            return new ResponseApi("Get student success", listData, countObject);
        } catch (Exception e) {
            return new ResponseApi("Get student error", null, 0);
        }
    }

    @Override
    public ResponseApi addStudent(Student student) {
        try {
            studentList.add(student);
            return new ResponseApi("Add student success", null, 0);
        } catch (Exception e) {
            return new ResponseApi("Add student error", null, 0);
        }
    }

    @Override
    public ResponseApi updateStudent(int index, Student student) {
        try {
            studentList.set(index, student);
            return new ResponseApi("Update student success", null, 0);
        } catch (Exception e) {
            return new ResponseApi("Update student error", null, 0);
        }
    }

    @Override
    public ResponseApi deleteStudent(int index) {
        try {
            studentList.remove(index);
            return new ResponseApi("Delete student success", null, 0);
        } catch (Exception e) {
            return new ResponseApi("Delete student error", null, 0);
        }
    }

    @Override
    public ResponseApi addTeacher(Teacher teacher) {
        try {
            teacherList.add(teacher);
            return new ResponseApi("Add teacher success", null, 0);
        } catch (Exception e) {
            return new ResponseApi("Add teacher error", null, 0);
        }
    }

    @Override
    public ResponseApi updateTeacher(int index, Teacher teacher) {
        try {
            teacherList.set(index, teacher);
            return new ResponseApi("Update teacher success", null, 0);
        } catch (Exception e) {
            return new ResponseApi("Update teacher error", null, 0);
        }
    }

    @Override
    public ResponseApi deleteTeacher(int index) {
        try {
            teacherList.remove(index);
            return new ResponseApi("Delete teacher success", null, 0);
        } catch (Exception e) {
            return new ResponseApi("Delete teacher error", null, 0);
        }
    }
}
