package com.example.minhha_21._bai61.service;

import com.example.minhha_21._bai61.edu.Student;
import com.example.minhha_21._bai61.edu.Teacher;
import com.example.minhha_21._bai61.model.ResponseApi;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface Services {
    public ResponseApi getPerson();
    public ResponseApi addStudent(Student student);
    public ResponseApi updateStudent(int index, Student student);
    public ResponseApi deleteStudent(int index);
    public ResponseApi addTeacher(Teacher teacher);
    public ResponseApi updateTeacher(int index, Teacher teacher);
    public ResponseApi deleteTeacher(int index);
}
