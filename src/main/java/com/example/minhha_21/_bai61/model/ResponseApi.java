package com.example.minhha_21._bai61.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class ResponseApi {
    private String message;
    private Map<String, List<?>> listData;
    private int countObject;
}
